//if else
let numA = 5;
if(numA > 3) {
    console.log('Hello');
}

let city = "New York";

if(city == "New York"){
    console.log("Welcome to New York");
}

if (numA < 3){
    console.log("Hello");
}
else if (numA > 3) {
    console.log("World");
}

//else statement

let numC = -5;
let numD = 7;

if (numC > 0){
    console.log("hello");
} else if (numD == 0)
{
    console.log("world");
} else {
    console.log("again")
}

console.warn('message');

// conditional ternary operator

let ternaryResult = (1<18) ? true : false;
console.log("result of ternary operator: " + ternaryResult);

console.log(parseInt("550") == 550)
//parseString

//document.write type sa browser

//switch

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}

//toLowerCase() + toUpperCase()